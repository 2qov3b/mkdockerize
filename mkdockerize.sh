#!/bin/bash

OPS="produce, serve"

for op in $OPS
do
    if [[ $1 == 'produce' ]]; then
        docker run -v $(pwd)/data:/root/tmp registry.gitlab.com/2qov3b/mkdockerize:latest produce
    fi
    
    if [[ $1 == 'serve' ]]; then
        docker run -p 8000:8000 -v $(pwd)/data:/root/tmp registry.gitlab.com/2qov3b/mkdockerize:latest serve
    fi
done
