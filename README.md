# mkdockerize
First of all, thanks for inviting me to take the challenge, it is fun and in this challenge I implemented `.gitlab-ci.yml`, `Dockerfile`, `entrypoint.sh` and `mkdockerize.sh` in _4 hours_, spent _10 hours_ for testing and _2 hours_ making documentation.

Here is the [Git project](https://gitlab.com/2qov3b/mkdockerize) and the [Docker image](https://gitlab.com/2qov3b/mkdockerize/container_registry/) which I built.

I want to choose the best solution I can implement in the time given, and please feel free to discuss more details with me in the interview.


# Details
## Producing the website
I want to spin up an image that will automatically install Mkdocs and run a startup script, please check my `Dockerfile` and the `entrypoint.sh` script,
```shell
FROM python:alpine

RUN pip install --upgrade pip \
  && pip install mkdocs 

WORKDIR    /root/

EXPOSE 8000

COPY entrypoint.sh /root/entrypoint.sh
ENTRYPOINT ["sh", "/root/entrypoint.sh"]
```
and the script can provide options to produce and serve a website, 
```console
#!/bin/bash

OPS="produce, serve"

for op in $OPS
do
    if [[ $1 == 'produce' ]]; then
        mkdocs new project
        cd project
        mkdocs build
        tar -czvf archive.tar.gz site
        mkdir -p /root/tmp
        cp /root/project/archive.tar.gz /root/tmp
        exit
    fi
    
    if [[ $1 == 'serve' ]]; then
        mkdocs new project
        tar -xzvf /root/tmp/archive.tar.gz -C /root/project
        cd project
        mkdocs serve --dev-addr=0.0.0.0:8000
    fi
done
```
I already built the image [here](https://gitlab.com/2qov3b/mkdockerize/container_registry/) and you can use `registry.gitlab.com/2qov3b/mkdockerize:latest` as the-docker-image-name. 

Firstly, check your currect directory like,
```console
$ ls -ltra
total 32
drwxr-xr-x  16 xxx  xxx  512 Mar 29 07:38 ..
-rw-r--r--   1 xxx  xxx  193 Mar 29 07:38 README.md
-rw-r--r--   1 xxx  xxx  744 Mar 30 15:54 .gitlab-ci.yml
-rw-r--r--   1 xxx  xxx  190 Mar 30 15:54 Dockerfile
drwxr-xr-x   7 xxx  xxx  224 Mar 30 15:54 .
-rw-r--r--   1 xxx  xxx  491 Mar 30 15:54 entrypoint.sh
drwxr-xr-x  15 xxx  xxx  480 Mar 30 15:54 .git
```
And then you can execute the following command to verify the produce task,
```console
docker run -v $(pwd)/data:/root/tmp registry.gitlab.com/2qov3b/mkdockerize:latest produce
```
Here I want to write out `archive.tar.gz` from container's /root/tmp to local $(pwd)/data which the arguments are `-v $(pwd)/data:/root/tmp`.

You can observe that container is created and the static website contents compressed as tarball,
```console
.
.
.
xxx: Pull complete
xxx: Pull complete
xxx: Pull complete
xxx: Pull complete
Digest: sha256:xxx
Status: Downloaded newer image for registry.gitlab.com/2qov3b/mkdockerize:latest
INFO    -  Creating project directory: project
INFO    -  Writing config file: project/mkdocs.yml
INFO    -  Writing initial docs: project/docs/index.md
INFO    -  Cleaning site directory
INFO    -  Building documentation to directory: /root/project/site
INFO    -  Documentation built in 0.07 seconds
site/
site/404.html
site/fonts/
site/fonts/glyphicons-halflings-regular.woff
site/fonts/glyphicons-halflings-regular.eot
site/fonts/fontawesome-webfont.eot
site/fonts/glyphicons-halflings-regular.woff2
site/fonts/fontawesome-webfont.woff2
.
.
.
```
Now check the ./data, you will get the tar.gz and that is the produce task.
```console
$ ls ./data/
archive.tar.gz
```

## Running the website
Next you can try command here to check the serve task.

```console
$ docker run -p 8000:8000 -v $(pwd)/data:/root/tmp registry.gitlab.com/2qov3b/mkdockerize:latest serve
```

I want to copy the archive.tar.gz file in $(pwd)/data to container's /root/tmp, and start the Mkdocs server.
```console
.
.
.
site/img/favicon.ico
site/js/
site/js/bootstrap.min.js
site/js/jquery-1.10.2.min.js
site/js/base.js
site/index.html
INFO    -  Building documentation...
INFO    -  Cleaning site directory
INFO    -  Documentation built in 0.07 seconds
[I 200330 08:51:25 server:296] Serving on http://0.0.0.0:8000
INFO    -  Serving on http://0.0.0.0:8000
[I 200330 08:51:25 handlers:62] Start watching changes
INFO    -  Start watching changes
[I 200330 08:51:25 handlers:64] Start detecting changes
INFO    -  Start detecting changes
```
You can check the connnection by curl or simply open the website.
```console
$ curl -I http://localhost:8000
HTTP/1.1 200 OK
Server: TornadoServer/6.0.4
Content-Type: text/html
Date: Mon, 30 Mar 2020 10:35:42 GMT
Accept-Ranges: bytes
Etag: "9a90e011b69f0b4e1c30bdce89af2aec"
Last-Modified: Mon, 30 Mar 2020 08:51:25 GMT
Content-Length: 6906
```


And about the wrapper script `mkdockerize.sh`. Of course we can do more flexible design on it but before jumping to optimize the script, let me test the basic options: produce and serve.

The script is simply combining the docker command we tested before.
```console
#!/bin/bash

OPS="produce, serve"

for op in $OPS
do
    if [[ $1 == 'produce' ]]; then
        docker run -v $(pwd)/data:/root/tmp registry.gitlab.com/2qov3b/mkdockerize:latest produce
    fi
    
    if [[ $1 == 'serve' ]]; then
        docker run -p 8000:8000 -v $(pwd)/data:/root/tmp registry.gitlab.com/2qov3b/mkdockerize:latest serve
    fi
done
```

And when you executing,
```console
$ chmod 700 mkdockerize.sh 
$ ./mkdockerize.sh produce
```

You will get the same result as `docker run -v $(pwd)/data:/root/tmp registry.gitlab.com/2qov3b/mkdockerize:latest produce`

And run
```console
$ ./mkdockerize.sh serve
```

You shold get the website, but please remember _docker stop_ the container which is using port 8000 before you run it.

In the `.gitlab-ci.yml` I want to use docker-in-docker to build my job and test the command by using [Predefined environment variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html#variables-reference)
```console
image: docker:19.03
services:
  - docker:19.03-dind

stages:
  - build
  - test

variables:
  IMAGE_TAG: $CI_REGISTRY_IMAGE:latest

before_script:
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  - echo '172.17.0.4 localhost' >> /etc/hosts

build:
  stage: build
  script:
    - docker build --pull -t $IMAGE_TAG .
    - docker push $IMAGE_TAG

test_produce_serve:
  stage: test
  script:
    - echo "Start to PRODUCE!"
    - docker run -v $(pwd)/data:/root/tmp $CI_REGISTRY_IMAGE produce
    - echo "Finish..."
    - ls -ltr data
    - echo "Start to SERVE!"
    - cat /etc/hosts
    - docker run -p 8000:8000 -d -v $(pwd)/data:/root/tmp $CI_REGISTRY_IMAGE serve
    - ifconfig
    - netstat -p
    - docker ps -a
```

Please check [Pipelines](https://gitlab.com/2qov3b/mkdockerize/pipelines) and finally I can check the produce/serve jobs are successfully tested.
