FROM python:alpine

RUN pip install --upgrade pip \
  && pip install mkdocs 

WORKDIR    /root/

EXPOSE 8000

COPY entrypoint.sh /root/entrypoint.sh
ENTRYPOINT ["sh", "/root/entrypoint.sh"]
