#!/bin/bash

OPS="produce, serve"

for op in $OPS
do
    if [[ $1 == 'produce' ]]; then
        mkdocs new project
        cd project
        mkdocs build
        tar -czvf archive.tar.gz site
        mkdir -p /root/tmp
        cp /root/project/archive.tar.gz /root/tmp
        exit
    fi
    
    if [[ $1 == 'serve' ]]; then
        mkdocs new project
        tar -xzvf /root/tmp/archive.tar.gz -C /root/project
        cd project
        mkdocs serve --dev-addr=0.0.0.0:8000
    fi
done
